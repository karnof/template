<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController01@index');
Route::get('/form', 'FormController01@bio');
Route::post('/welcome', 'FormController01@kirim');
route::get('/data-table', function(){
    return view('table.datatable');
});
route::get('/table', function(){
    return view('table.table');
});